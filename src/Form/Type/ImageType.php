<?php

namespace dlouhy\ImageBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use dlouhy\ImageBundle\Entity\Intervencionalist;

class ImageType extends AbstractType
{

	
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
				->add('translations', 'a2lix_translations', array(
					'label' => false,
					'fields' => array(
						'description' => array(
							'label' => 'Popis'
				))))
				->add('position', 'integer', array(
					'label' => 'Priorita řazení'
		));
	}


	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'dlouhy\ImageBundle\Entity\Image'
		));
	}


	public function getName()
	{
		return 'image';
	}

}
