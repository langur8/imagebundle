<?php

namespace dlouhy\ImageBundle\Controller;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ImageController
{
	
	/**
	 * @var Registry
	 */
	private $doctrine;
	
	public function __construct(Registry $doctrine)
	{
		$this->doctrine = $doctrine;
	}
	
    public function imageAction(Request $request, $gallery, $size, $file)
    {
		$path = pathinfo($file);
		$filename = $path['filename'];
		$suffix = $path['extension'];
		
		//todo - images cely presounout do image bundle
        $imageRepo = $this->doctrine->getRepository('dlouhy\ImageBundle\Entity\Image');
		$image = $imageRepo->findByNameAndGallery($filename, $suffix, $gallery);				
		
		if($image === null) {
			throw new NotFoundHttpException('Image not found');
		}
									
		if($size === 'or') {
			$size = null;
		}
		
		$fpath = $image->getAbsolutePath($size);
		
		if(!file_exists($fpath)) {
			throw new NotFoundHttpException('Image not found');
		}
				
		$sha1 = $request->get('sha1');		
		if($sha1 !== null && $sha1 !== sha1_file($fpath)) {
			throw new NotFoundHttpException('Checksum not match');
		}
		
		$response = new Response;		
		$response->headers->set('Content-Type', $image->getMime());
		$response->setContent(file_get_contents($fpath));

		return $response;	
    }
}
