<?php
namespace dlouhy\ImageBundle\Entity;

/**
 * Entita obrazku
 *
 * @author Vaclav Dlouhy <vdlouhy@atlas.cz>
 */


use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Criteria;

/**
 * @ORM\Entity
 * @ORM\Table(name="img__image_galleries", indexes={ 
 * 		@ORM\Index(name="idx_search", columns={"folder"}),
 * 		@ORM\Index(name="idx_active", columns={"active"}),
 * 		@ORM\Index(name="idx_deleted", columns={"deleted"}),
 * 		@ORM\Index(name="idx_modified", columns={"modified"})
 * 		})
 * @ORM\HasLifecycleCallbacks()
 *
 */
class ImageGallery extends EntityAbstract
{

	/**
	 * @var int
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="datetime")
	 */
	protected $created;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	protected $modified;

	/**
	 * @var bool
	 * @ORM\Column(type="boolean")
	 */
	protected $active = true;

	/**
	 * @var bool
	 * @ORM\Column(type="boolean")
	 */
	protected $deleted = false;

	/**
	 * @var string
	 * @ORM\Column(type="string", length=90, nullable=true)
	 */
	protected $name;

	/**
	 * @var string
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $folder;

	/**
	 * @var \Doctrine\Common\Collections\ArrayCollection
	 * @ORM\OneToMany(targetEntity="Image", mappedBy="imageGallery", orphanRemoval=true, cascade={"persist", "remove"})
	 * @ORM\OrderBy({"position" = "DESC"})
	 */
	protected $images;

	/**
	 * @var Image
	 * @ORM\OneToOne(targetEntity="Image")
	 * @ORM\JoinColumn(name="home_image_id", referencedColumnName="id", onDelete="SET NULL")
	 */
	protected $homeImage;

	/**
	 * @var Image
	 * @ORM\OneToOne(targetEntity="Image")
	 * @ORM\JoinColumn(name="list_thumbnail_image_id", referencedColumnName="id", onDelete="SET NULL")
	 */
	protected $listThumbnailImage;

	/**
	 * @ORM\PreUpdate
	 */
	public function preUpdate()
	{
		$this->modified = new \DateTime;
	}


	/**
	 * @ORM\PrePersist
	 */
	public function prePersist()
	{
		$this->created = new \DateTime;
	}


	/**
	 * Sets a folder
	 */
	public function setUniqueFolder()
	{
		$this->setFolder(sha1(uniqid(mt_rand(), true)));
	}

	/**
	 * Vybere prvni obrazek jako hlavni, pokud neni nstaven
	 */
	public function setDefaults($images = null)
	{
		
		if(!$images instanceof \Doctrine\Common\Collections\Collection) {
			$images = $this->images;
		}
		
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq('deleted', false));
		
		$first = $images->matching($criteria)->first();									
		
		if ($this->getHomeImage() === null && $this->getImages()->count() > 0) {					
			$this->setHomeImage($first === false ? null : $first);
		}

//		if ($this->getListThumbnailImage() === null && $this->getImages()->count() > 0) {
//			$this->setListThumbnailImage($first === false ? null : $first);
//		}
	}


	/**
	 * gallerie bez home image
	 * @return array
	 */
	public function getGallery()
	{
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq('deleted', false));		
		
		$gallery = $this->images->matching($criteria)->toArray();
		if ($this->getHomeImage() !== null) {
			foreach ($gallery as $key => $image) {
				if ($image->getId() === $this->getHomeImage()->getId()) {
					unset($gallery[$key]);
				}
			}
		}
		return $gallery;
	}

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPresentImages()
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq('deleted', false));

        return $this->images->matching($criteria);
    }


//GENERATED
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->images = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return ImageGallery
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return ImageGallery
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return ImageGallery
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     * @return ImageGallery
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean 
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ImageGallery
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set folder
     *
     * @param string $folder
     * @return ImageGallery
     */
    public function setFolder($folder)
    {
        $this->folder = $folder;

        return $this;
    }

    /**
     * Get folder
     *
     * @return string 
     */
    public function getFolder()
    {
        return $this->folder;
    }

    /**
     * Add images
     *
     * @param \dlouhy\ImageBundle\Entity\Image $images
     * @return ImageGallery
     */
    public function addImage(\dlouhy\ImageBundle\Entity\Image $images)
    {
        $this->images[] = $images;

        return $this;
    }

    /**
     * Remove images
     *
     * @param \dlouhy\ImageBundle\Entity\Image $images
     */
    public function removeImage(\dlouhy\ImageBundle\Entity\Image $images)
    {
        $this->images->removeElement($images);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Set homeImage
     *
     * @param \dlouhy\ImageBundle\Entity\Image $homeImage
     * @return ImageGallery
     */
    public function setHomeImage(\dlouhy\ImageBundle\Entity\Image $homeImage = null)
    {
        $this->homeImage = $homeImage;

        return $this;
    }

    /**
     * Get homeImage
     *
     * @return \dlouhy\ImageBundle\Entity\Image 
     */
    public function getHomeImage()
    {
        return $this->homeImage;
    }

    /**
     * Set listThumbnailImage
     *
     * @param \dlouhy\ImageBundle\Entity\Image $listThumbnailImage
     * @return ImageGallery
     */
    public function setListThumbnailImage(\dlouhy\ImageBundle\Entity\Image $listThumbnailImage = null)
    {
        $this->listThumbnailImage = $listThumbnailImage;

        return $this;
    }

    /**
     * Get listThumbnailImage
     *
     * @return \dlouhy\ImageBundle\Entity\Image 
     */
    public function getListThumbnailImage()
    {
        return $this->listThumbnailImage;
    }
}
