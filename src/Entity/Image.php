<?php

/**
 * Entita obrazku
 *
 * @author Vaclav Dlouhy <vdlouhy@atlas.cz>
 */

namespace dlouhy\ImageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use dlouhy\UtilsBundle\Utils\Strings;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity(repositoryClass="dlouhy\ImageBundle\Entity\Repository\ImageRepository")
 * @ORM\Table(name="img__images", indexes={ 
 * 		@ORM\Index(name="idx_search", columns={"filename", "suffix"}),
 * 		@ORM\Index(name="idx_active", columns={"active"}),
 * 		@ORM\Index(name="idx_deleted", columns={"deleted"}),
 * 		@ORM\Index(name="idx_modified", columns={"modified"})
 * 		})
 * @ORM\HasLifecycleCallbacks()
 *
 */
class Image extends EntityAbstract
{

	use ORMBehaviors\Translatable\Translatable;	
	
	/**
	 * @var int
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="datetime")
	 */
	protected $created;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	protected $modified;

	/**
	 * @var bool
	 * @ORM\Column(type="boolean")
	 */
	protected $active = true;

	/**
	 * @var bool
	 * @ORM\Column(type="boolean")
	 */
	protected $deleted = false;

	/**
	 * @var string
	 * @ORM\Column(type="string", length=90, nullable=true)
	 */
	protected $name;

	/**
	 * @var string
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $filename;

	/**
	 * @var string
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $suffix;

	/**
	 * @var int
	 * @ORM\Column(type="integer", nullable=true)
	 */
	protected $position = 1;

	/**
	 * @var string
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $sha1;

	/**
	 * @var string
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $url;

	/**
	 * @var string
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $mime;

	/**
	 * @var string
	 * @ORM\Column(type="integer", nullable=true)
	 */
	protected $size;

	/**
	 * @var UploadedFile
	 * @Assert\File(maxSize="6000000")
	 */
	private $file;

	/**
	 * @var ImageGallery
	 * @ORM\ManyToOne(targetEntity="ImageGallery", inversedBy="images")
	 * @ORM\JoinColumn(name="image_gallery_id", referencedColumnName="id", onDelete="CASCADE")
	 * */
	protected $imageGallery;


	/**
	 * @ORM\PreUpdate
	 */
	public function preUpdate()
	{
		$this->modified = new \DateTime;
	}


	/**
	 * @ORM\PrePersist
	 */
	public function prePersist()
	{
		$this->created = new \DateTime;
	}


	public function getAbsolutePath($filter = null)
	{
		return null === $this->filename ? null : $this->getUploadRootDir() . '/' . $this->getPath($filter);
	}


	public function getWebPath($filter = null)
	{
		return null === $this->filename ? null : '/' . $this->getUploadDir() . '/' . $this->getImageGallery()->getFolder() . '/' . $this->getPath($filter);
	}


	public function getUploadRootDir()
	{
		// the absolute directory path where uploaded
		// documents should be saved
		return __DIR__ . '/../../../../../web/' . $this->getUploadDir() . '/' . $this->getImageGallery()->getFolder();
	}


	public function getPath($filter = null)
	{
		return $this->filename . ($filter ? '-' . $filter : '' ) . '.' . $this->suffix;
	}


	protected function getUploadDir()
	{
		// get rid of the __DIR__ so it doesn't screw up
		// when displaying uploaded doc/image in the view.
		return 'uploads/images';
	}


	private function generateFilename()
	{

		$basename = Strings::Slug(
						preg_replace('/\\.[^.\\s]{3,4}$/', '', $this->getFile()->getClientOriginalName())
		);

		$suffix = $this->getFile()->guessExtension();

		$i = 1;
		$filename = $basename;
		while (file_exists($this->getUploadRootDir() . '/' . $filename . '.' . $suffix)) {
			$filename = $basename . '-' . $i;
			$i++;
		}
		$this->setFilename($filename);
		$this->setSuffix($suffix);
	}


	/**
	 * Sets file.
	 *
	 * @param UploadedFile $file
	 */
	public function setFile(UploadedFile $file = null)
	{
		$this->file = $file;
		// check if we have an old image path
		if (isset($this->filename)) {
			// store the old name to delete after the update
			$this->temp = $this->filename;
			$this->filename = null;
		} else {
			$this->filename = 'initial';
		}
	}


	/**
	 * Get file.
	 *
	 * @return UploadedFile
	 */
	public function getFile()
	{
		return $this->file;
	}


	/**
	 * @ORM\PrePersist()
	 * @ORM\PreUpdate()
	 */
	public function preUpload()
	{
		if (null !== $this->getFile()) {
			$this->generateFilename();
			$this->setSha1(sha1_file($this->getFile()->getPathname()));
			$this->setMime($this->getFile()->getMimeType());
			$this->setSize($this->getFile()->getSize());
		}
	}


	/**
	 * @ORM\PostPersist()
	 * @ORM\PostUpdate()
	 */
	public function upload()
	{
		if (null === $this->getFile()) {
			return;
		}

		// if there is an error when moving the file, an exception will
		// be automatically thrown by move(). This will properly prevent
		// the entity from being persisted to the database on error
		$this->getFile()->move($this->getUploadRootDir(), $this->getPath());

		// check if we have an old image
		if (isset($this->temp)) {
			// delete the old image
			unlink($this->getUploadRootDir() . '/' . $this->temp);
			// clear the temp image path
			$this->temp = null;
		}
		$this->file = null;
	}


	/**
	 * @ORM\PostRemove()
	 */
	public function removeUpload()
	{
		$file = str_replace('__deleted__', '', $this->getAbsolutePath());
		if (file_exists($file)) {
			unlink($file);
		}
	}


	public function getFilenameWithSuffix()
	{
		return $this->getFilename() . '.' . $this->getSuffix();
	}

	
	public function __call($method, $arguments)
	{
		return $this->proxyCurrentLocaleTranslation($method, $arguments);
	}	
	
//GENERATED


	/**
	 * Get id
	 *
	 * @return integer 
	 */
	public function getId()
	{
		return $this->id;
	}


	/**
	 * Set created
	 *
	 * @param \DateTime $created
	 * @return Image
	 */
	public function setCreated($created)
	{
		$this->created = $created;

		return $this;
	}


	/**
	 * Get created
	 *
	 * @return \DateTime 
	 */
	public function getCreated()
	{
		return $this->created;
	}


	/**
	 * Set modified
	 *
	 * @param \DateTime $modified
	 * @return Image
	 */
	public function setModified($modified)
	{
		$this->modified = $modified;

		return $this;
	}


	/**
	 * Get modified
	 *
	 * @return \DateTime 
	 */
	public function getModified()
	{
		return $this->modified;
	}


	/**
	 * Set active
	 *
	 * @param boolean $active
	 * @return Image
	 */
	public function setActive($active)
	{
		$this->active = $active;

		return $this;
	}


	/**
	 * Get active
	 *
	 * @return boolean 
	 */
	public function getActive()
	{
		return $this->active;
	}


	/**
	 * Set deleted
	 *
	 * @param boolean $deleted
	 * @return Image
	 */
	public function setDeleted($deleted)
	{
		$this->deleted = $deleted;

		return $this;
	}


	/**
	 * Get deleted
	 *
	 * @return boolean 
	 */
	public function getDeleted()
	{
		return $this->deleted;
	}


	/**
	 * Set name
	 *
	 * @param string $name
	 * @return Image
	 */
	public function setName($name)
	{
		$this->name = $name;

		return $this;
	}


	/**
	 * Get name
	 *
	 * @return string 
	 */
	public function getName()
	{
		return $this->name;
	}


	/**
	 * Set filename
	 *
	 * @param string $filename
	 * @return Image
	 */
	public function setFilename($filename)
	{
		$this->filename = $filename;

		return $this;
	}


	/**
	 * Get filename
	 *
	 * @return string 
	 */
	public function getFilename()
	{
		return $this->filename;
	}


	/**
	 * Set suffix
	 *
	 * @param string $suffix
	 * @return Image
	 */
	public function setSuffix($suffix)
	{
		$this->suffix = $suffix;

		return $this;
	}


	/**
	 * Get suffix
	 *
	 * @return string 
	 */
	public function getSuffix()
	{
		return $this->suffix;
	}


	/**
	 * Set position
	 *
	 * @param integer $position
	 * @return Image
	 */
	public function setPosition($position)
	{
		$this->position = $position;

		return $this;
	}


	/**
	 * Get position
	 *
	 * @return integer 
	 */
	public function getPosition()
	{
		return $this->position;
	}


	/**
	 * Set sha1
	 *
	 * @param string $sha1
	 * @return Image
	 */
	public function setSha1($sha1)
	{
		$this->sha1 = $sha1;

		return $this;
	}


	/**
	 * Get sha1
	 *
	 * @return string 
	 */
	public function getSha1()
	{
		return $this->sha1;
	}


	/**
	 * Set url
	 *
	 * @param string $url
	 * @return Image
	 */
	public function setUrl($url)
	{
		$this->url = $url;

		return $this;
	}


	/**
	 * Get url
	 *
	 * @return string 
	 */
	public function getUrl()
	{
		return $this->url;
	}


	/**
	 * Set mime
	 *
	 * @param string $mime
	 * @return Image
	 */
	public function setMime($mime)
	{
		$this->mime = $mime;

		return $this;
	}


	/**
	 * Get mime
	 *
	 * @return string 
	 */
	public function getMime()
	{
		return $this->mime;
	}


	/**
	 * Set size
	 *
	 * @param integer $size
	 * @return Image
	 */
	public function setSize($size)
	{
		$this->size = $size;

		return $this;
	}


	/**
	 * Get size
	 *
	 * @return integer 
	 */
	public function getSize()
	{
		return $this->size;
	}


	/**
	 * Set imageGallery
	 *
	 * @param \dlouhy\ImageBundle\Entity\ImageGallery $imageGallery
	 * @return Image
	 */
	public function setImageGallery(\dlouhy\ImageBundle\Entity\ImageGallery $imageGallery = null)
	{
		$this->imageGallery = $imageGallery;

		return $this;
	}


	/**
	 * Get imageGallery
	 *
	 * @return \dlouhy\ImageBundle\Entity\ImageGallery 
	 */
	public function getImageGallery()
	{
		return $this->imageGallery;
	}

}
