<?php
namespace dlouhy\ImageBundle\Service;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Liip\ImagineBundle\Imagine\Data\DataManager;
use Liip\ImagineBundle\Imagine\Filter\FilterManager;
use dlouhy\ImageBundle\Entity\Image;
use dlouhy\ImageBundle\Entity\ImageGallery;

class ImageService
{
	
	/**
	 * @var Registry
	 */
	private $doctrine;

	/**
	 * @var DataManager
	 */
	private $dataManager;

	/**
	 * @var FilterManager
	 */
	private $filterManager;

	/**
	 * Resizing def
	 * @var array
	 */
	private $filters = array('sm', 'md', 'lg', 'sm_sq', 'md_sq', 'lg_sq');

	public function __construct(Registry $doctrine, DataManager $liipDataManager, FilterManager $liipFilterManager)
	{
		$this->dataManager = $liipDataManager;
		$this->filterManager = $liipFilterManager;
		$this->doctrine = $doctrine;
	}


	private function getDoctrine()
	{
		return $this->doctrine;
	}


	public function upload(UploadedFile $file, $gallery = null)
	{
		$image = new Image;
		$image->setFile($file);
		$em = $this->getDoctrine()->getManager();		
		
		if ($gallery instanceof ImageGallery) {			
			$image->setImageGallery($gallery);
			$em->persist($image);
			
			$gallery->addImage($image);
			$gallery->setDefaults();	
			$em->persist($gallery);	
		} else {
			$em->persist($image);
		}

		$em->flush();

		foreach ($this->filters as $filter) {
			$this->writeThumbnail($image, $filter);
		}
	}
	
	
	public function uploadToGallery(UploadedFile $file, ImageGallery $gallery = null, $singleImageGallery = false)	
	{
		if(!$gallery instanceof ImageGallery) {
			$gallery = new ImageGallery;
			$gallery->setUniqueFolder();
		}
		
		if($singleImageGallery === true) {
			$this->deleteGalleryImages($gallery);
		}				
		
		$this->upload($file, $gallery);
		
		return $gallery;
	}


	public function delete(Image $image)
	{
		$gallery = $image->getImageGallery();
		
		$em = $this->getDoctrine()->getManager();
		$em->remove($image);
		$em->flush();
		
		if ($gallery instanceof ImageGallery) {
			if ($gallery->getHomeImage() !== null && $gallery->getHomeImage()->getId() === $image->getId()) {
				$gallery->setHomeImage(null);
			}
			if ($gallery->getListThumbnailImage() !== null && $gallery->getListThumbnailImage()->getId() === $image->getId()) {
				$gallery->setListThumbnailImage(null);
			}			
			$gallery->removeImage($image);			
			$gallery->setDefaults();
			$em->persist($gallery);
			$em->flush();
		}		

		foreach ($this->filters as $filter) {
			$this->deleteThumbnail($image, $filter);
		}
		$this->deleteThumbnail($image, 'or');
	}
	
	
	public function deleteGalleryImages(ImageGallery $gallery) 
	{
		foreach($gallery->getPresentImages()->getIterator() as $image) {
			$this->delete($image);
		}
	}


	private function writeThumbnail(Image $image, $filter)
	{
		$path = $image->getWebPath();
		$tpath = $image->getUploadRootDir() . '/' . $image->getPath($filter);

		$lImage = $this->dataManager->find($filter, $path);
		$response = $this->filterManager->applyFilter($lImage, $filter);
		$thumb = $response->getContent();

		$f = fopen($tpath, 'w');
		fwrite($f, $thumb);
		fclose($f);
	}


	private function deleteThumbnail(Image $image, $filter)
	{
		$tpath = $image->getUploadRootDir() . '/' . $image->getPath($filter);

		if (file_exists($tpath)) {
			unlink($tpath);
		}
	}


}
