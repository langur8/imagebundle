<?php

namespace dlouhy\ImageBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Doctrine\ORM\EntityRepository;

class ImageGalleryType extends AbstractType
{

	protected $entity;
	
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		
		$entity = $options['data'];	
		
        $builder->add('images', 'collection', array(
            'type' => new ImageType(),
            'label' => false,
            'allow_add' => false,
            'allow_delete' => false,
            'by_reference' => false
        ));
				
		
		if($entity->getId()) {		
		
        $builder->add('homeImage', 'entity', array(
			'class' => 'dlouhy\ImageBundle\Entity\Image',
			//'data_class' => 'dlouhy\ImageBundle\Entity\Image',
			'expanded' => true,
			'multiple' => false,
			'property' => function() {return 'Titulní obrázek galerie';},
			'label' => false,
			'query_builder' => function(EntityRepository $er) use ($entity){
				return $er->createQueryBuilder('i')->where('i.imageGallery = ?1')->andWhere('i.deleted = ?2')->setParameters(array(1 => $entity, 2 => 0));
				}			
		));
			
		}
		
		if($entity->getPresentImages()->count() > 0) {
			$builder->add('save', 'submit', array('label' => 'OK'));
		}				  
		
	}
	
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'required' => false,
			'data_class' => 'dlouhy\ImageBundle\Entity\ImageGallery'
		));
	}


	public function getName()
	{
		return 'image_gallery';
	}


}
